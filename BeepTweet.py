#!/usr/bin/env python3
# using SendGrid's Python Library
# https://github.com/sendgrid/sendgrid-python
import os
import sendgrid
from sendgrid.helpers.mail import Mail, Email, To, Content
import http.client
import json
import time
import re
import datetime


absolute_path = os.path.dirname(os.path.abspath(__file__))
config_path = absolute_path + '/config.json'
with open(config_path) as json_data_file:
    config = json.load(json_data_file)


i = 0
x = 0
message = []
user = []
idDict = {}
time = time = (datetime.datetime.utcnow() - datetime.timedelta(minutes=config["Settings"]["Interval"])).strftime('%Y-%m-%dT%H:%M:%SZ') # Calculates the UTC time stamp of now - the interval of the cron job, so we can look only for new tweets.
print ("Current Time Is: "+datetime.datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ'))
print ("Fetching Mentions Since: "+time)

# This section, stolen from stack overflow, stripes emotes, as they breaks things
def deEmojify(text):
    regrex_pattern = re.compile(pattern = "["
        u"\U0001F600-\U0001F64F"  # emoticons
        u"\U0001F300-\U0001F5FF"  # symbols & pictographs
        u"\U0001F680-\U0001F6FF"  # transport & map symbols
        u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
        u"\U00002500-\U00002BEF"  # chinese char
        u"\U00002702-\U000027B0"
        u"\U00002702-\U000027B0"
        u"\U000024C2-\U0001F251"
        u"\U0001f926-\U0001f937"
        u"\U00010000-\U0010ffff"
        u"\u2640-\u2642"
        u"\u2600-\u2B55"
        u"\u200d"
        u"\u23cf"
        u"\u23e9"
        u"\u231a"
        u"\ufe0f"  # dingbats
        u"\u3030"
                           "]+", flags = re.UNICODE)
    return regrex_pattern.sub(r'',text)

# This section connects to Twitter and fetches all mentions since the last run then parses the response into a JSON object.
conn = http.client.HTTPSConnection("api.twitter.com")
payload = ''
headers = {
  'Authorization': 'Bearer '+config["Twitter"]["Token"],
  'Cookie': 'guest_id='+config["Twitter"]["guest_id"]+'; personalization_id='+'"'+config["Twitter"]["personalization_id"]+'"'
}
conn.request("GET", "/2/users/"+config["Twitter"]["User_ID"]+"/mentions?start_time="+time+"&expansions=author_id&user.fields=name", payload, headers)
res = conn.getresponse()
data = res.read()
parsedData = json.loads(data)

try: # If there are no users returned, that means no tweets where returned.
    while x < len(parsedData["includes"]["users"]): # Loop through all the returned users and build a dictionary to associate UserID to UserName (Because the list of usernames from the API only has one entry per username even if there are multiple tweets from that user)
        idDict[parsedData["includes"]["users"][x]["id"]] = parsedData["includes"]["users"][i]["username"]
        x+=1
except:
    print("No new tweets.")
    exit()


if config["Settings"]["Pager_Email"] == "N/A": # If not useing email, use sms
    try: # If anything else fails, there was an error.
        while i < len(parsedData["data"]): # This loops through every returned tweet.
            #message.append(parsedData["data"][i]["text"])
            user.append(parsedData["data"][i]["author_id"]) # Look up the author ID and find the username.
            # This section handles Twillio and sending the message.
            twillioConn = http.client.HTTPSConnection("api.twilio.com")
            twillioPayload = 'Body='+"User: @"+idDict[user[i]]+" Said: "+deEmojify(parsedData["data"][i]["text"])+'&To='+config["Settings"]["Pager_Number"]+'&From='+config["Twillio"]["FromNumber"]
            twillioHeaders = {
              'Content-Type': 'application/x-www-form-urlencoded',
              'Authorization': 'Basic '+config["Twillio"]["Key"]
            }
            twillioConn.request("POST", "/2010-04-01/Accounts/"+config["Twillio"]["AccountSID"]+"/Messages.json", twillioPayload, twillioHeaders)
            i+=1
        print("SMS Success!")
    except:
        print("SMS Error")
else:
    print ("Using Email")
    i = 0
    while i < len(parsedData["data"]): # This loops through every returned tweet.
        #message.append(parsedData["data"][i]["text"])
        user.append(parsedData["data"][i]["author_id"]) # Look up the author ID and find the username.

        sg  = sendgrid.SendGridAPIClient(api_key=config["Sendgrid"]["API_key"])
        from_email = Email(config["Sendgrid"]["From_email"])  # Change to your verified sender
        to_email = To(config["Settings"]["Pager_Email"])  # Change to your recipient
        subject = "BeepTweet Alert"
        content = Content("text/plain", "User: @"+idDict[user[i]]+" Said: "+deEmojify(parsedData["data"][i]["text"]))
        mail = Mail(from_email, to_email, subject, content)

        # Get a JSON-ready representation of the Mail object
        mail_json = mail.get()

        # Send an HTTP POST request to /mail/send
        response = sg.client.mail.send.post(request_body=mail_json)
        print(response.status_code)
        print(response.headers)
        i+=1


# ToDo:
#   1. Implement Email with sendgrid
#   2. Put in some code to scrub the actual @username from the message text for space savings?
