# BeepTweet

My code for the program to send my mentions to my pager aka beeper as seen in this tweet: https://twitter.com/hime0698/status/1448437222819172357?s=20

Could also be used for sending to anything that can receive SMS. Eventually email as well.

# Usage

This script is intended to be periodically executed via cron.

API Keys, phone numbers, etc are in the config.json file.

The Interval setting should be set to 30 seconds longer than the interval on the cron job that executes this. For example if your cron job runs the script every minute the interval should be set to 1.5
